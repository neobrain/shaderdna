const loop = `BB0
/* logical preds: / linear preds: / kind: uniform, top-level, loop-preheader, */
         s2: %18:s[0-1],  s1: %19:s[2],  s1: %20:s[3],  v2: %21:v[0-1],  s2: %22:exec = p_startpgm
        p_logical_start
         v1: %23:v[0],  v1: %24:v[1] = p_split_vector %21:v[0-1]
         s1: %81:m0 = p_parallelcopy %19:s[2]
         v1: %27:v[2] = v_interp_p1_f32 %23:v[0], %81:m0 attr0.x
         v1: %3:v[2] = v_interp_p2_f32 %24:v[1], %81:m0, %27:v[2] attr0.x
         v1: %28:v[3] = v_interp_p1_f32 %23:v[0], %81:m0 attr0.y
         v1: %4:v[3] = v_interp_p2_f32 %24:v[1], %81:m0, %28:v[3] attr0.y
        p_logical_end
         s2: %29:vcc = p_branch BB1
BB1
/* logical preds: BB0, BB17, / linear preds: BB0, BB17, / kind: loop-header, branch, */
         s2: %52:s[0-1] = p_linear_phi %22:exec, %52:s[0-1]
         s2: %53:exec = p_linear_phi %22:exec, %79:exec
         s1: %8:s[2] = p_linear_phi 0, %13:s[2]
         v1: %7:v[4] = p_phi %3:v[2], %12:v[4]
        p_logical_start
         v1: %9:v[5] = v_cvt_f32_i32 %8:s[2]
         s2: %10:vcc = v_cmp_ge_f32 %9:v[5], %3:v[2]
        p_logical_end
         s2: %56:s[4-5],  s1: %55:scc,  s2: %54:exec = s_and_saveexec_b64 %10:vcc, %53:exec
         s2: %57:vcc = p_cbranch_z %54:exec BB5, BB2
BB2
/* logical preds: BB1, / linear preds: BB1, / kind: break, */
        p_logical_start
        p_logical_end
         s2: %59:s[4-5],  s1: %58:scc = s_andn2_b64 %56:s[4-5], %54:exec
         s2: %60:vcc = p_cbranch_nz %58:scc BB4, BB3
BB3
/* logical preds: / linear preds: BB2, / kind: uniform, */
         s2: %32:vcc = p_branch BB18
BB4
/* logical preds: / linear preds: BB2, / kind: uniform, */
        p_logical_start
        p_logical_end
         s2: %33:vcc = p_branch BB6
BB5
/* logical preds: / linear preds: BB1, / kind: uniform, */
         s2: %34:vcc = p_branch BB6
BB6
/* logical preds: / linear preds: BB4, BB5, / kind: invert, */
         s2: %61:s[4-5] = p_linear_phi %59:s[4-5], %56:s[4-5]
         s2: %62:exec = p_linear_phi %54:exec, %54:exec
         s2: %64:exec,  s1: %63:scc = s_andn2_b64 %61:s[4-5], %62:exec
         s2: %65:vcc = p_cbranch_z %64:exec BB8, BB7
BB7
/* logical preds: BB1, / linear preds: BB6, / kind: uniform, */
        p_logical_start
        p_logical_end
         s2: %36:vcc = p_branch BB9
BB8
/* logical preds: / linear preds: BB6, / kind: uniform, */
         s2: %37:vcc = p_branch BB9
BB9
/* logical preds: BB7, / linear preds: BB7, BB8, / kind: branch, merge, */
         s2: %66:exec = p_parallelcopy %61:s[4-5]
        p_logical_start
         s2: %11:vcc = v_cmp_lt_f32 %4:v[3], %9:v[5]
        p_logical_end
         s2: %69:s[4-5],  s1: %68:scc,  s2: %67:exec = s_and_saveexec_b64 %11:vcc, %66:exec
         s2: %70:vcc = p_cbranch_z %67:exec BB13, BB10
BB10
/* logical preds: BB9, / linear preds: BB9, / kind: break, */
        p_logical_start
        p_logical_end
         s2: %72:s[4-5],  s1: %71:scc = s_andn2_b64 %69:s[4-5], %67:exec
         s2: %73:vcc = p_cbranch_nz %71:scc BB12, BB11
BB11
/* logical preds: / linear preds: BB10, / kind: uniform, */
         s2: %40:vcc = p_branch BB18
BB12
/* logical preds: / linear preds: BB10, / kind: uniform, */
        p_logical_start
        p_logical_end
         s2: %41:vcc = p_branch BB14
BB13
/* logical preds: / linear preds: BB9, / kind: uniform, */
         s2: %42:vcc = p_branch BB14
BB14
/* logical preds: / linear preds: BB12, BB13, / kind: invert, */
         s2: %74:s[4-5] = p_linear_phi %72:s[4-5], %69:s[4-5]
         s2: %75:exec = p_linear_phi %67:exec, %67:exec
         s2: %77:exec,  s1: %76:scc = s_andn2_b64 %74:s[4-5], %75:exec
         s2: %78:vcc = p_cbranch_z %77:exec BB16, BB15
BB15
/* logical preds: BB9, / linear preds: BB14, / kind: uniform, */
        p_logical_start
        p_logical_end
         s2: %44:vcc = p_branch BB17
BB16
/* logical preds: / linear preds: BB14, / kind: uniform, */
         s2: %45:vcc = p_branch BB17
BB17
/* logical preds: BB15, / linear preds: BB15, BB16, / kind: uniform, continue, merge, */
         s2: %79:exec = p_parallelcopy %74:s[4-5]
        p_logical_start
         v1: %12:v[4] = v_add_f32 1.0, %7:v[4]
         s1: %13:s[2],  s1: %46:scc = s_add_u32 %8:s[2], 1
        p_logical_end
         s2: %47:vcc = p_branch BB1
BB18
/* logical preds: BB2, BB10, / linear preds: BB3, BB11, / kind: uniform, top-level, loop-exit, export_end, */
         s2: %80:exec = p_parallelcopy %52:s[0-1]
        p_logical_start
         v1: %48:v[2] = v_interp_p1_f32 %23:v[0], %81:m0 attr0.w
         v1: %15:v[2] = v_interp_p2_f32 %24:v[1], %81:m0, %48:v[2] attr0.w
         v1: %49:v[0] = v_interp_p1_f32 %23:v[0], %81:m0 attr0.z
         v1: %16:v[0] = v_interp_p2_f32 %24:v[1], %81:m0, %49:v[0] attr0.z
         v1: %50:v[1] = v_cvt_pkrtz_f16_f32 %7:v[4], %4:v[3]
         v1: %51:v[0] = v_cvt_pkrtz_f16_f32 %16:v[0], %15:v[2]
        exp %50:v[1], %51:v[0],  v1: undef,  v1: undef en:rg** mrt0
        p_logical_end
        s_endpgm
`

export const simpleBranch = `BB0
/* logical preds: / linear preds: / kind: top-level, branch, */
         s2: %10:s[0-1],  s1: %11:s[2],  s1: %12:s[3],  v2: %13:v[0-1],  s2: %14:exec = p_startpgm
        p_logical_start
         v1: %15:v[0],  v1: %16:v[1] = p_split_vector %13:v[0-1]
         s1: %37:m0 = p_parallelcopy %11:s[2]
         v1: %19:v[0] = v_interp_p1_f32 %15:v[0], %37:m0 attr0.x
         v1: %4:v[0] = v_interp_p2_f32 %16:v[1], %37:m0, %19:v[0] attr0.x
         s2: %5:vcc = v_cmp_gt_f32 0.5, %4:v[0]
        p_logical_end
         s2: %30:s[0-1],  s1: %29:scc,  s2: %28:exec = s_and_saveexec_b64 %5:vcc, %14:exec
         s2: %31:vcc = p_cbranch_z %28:exec BB2, BB1
BB1
/* logical preds: BB0, / linear preds: BB0, / kind: uniform, */
        p_logical_start
        p_logical_end
         s2: %21:vcc = p_branch BB3
BB2
/* logical preds: / linear preds: BB0, / kind: uniform, */
         s2: %22:vcc = p_branch BB3
BB3
/* logical preds: / linear preds: BB1, BB2, / kind: invert, */
         s2: %32:exec = p_linear_phi %28:exec, %28:exec
         s2: %34:exec,  s1: %33:scc = s_andn2_b64 %30:s[0-1], %32:exec
         s2: %35:vcc = p_cbranch_z %34:exec BB5, BB4
BB4
/* logical preds: BB0, / linear preds: BB3, / kind: uniform, */
        p_logical_start
        p_logical_end
         s2: %24:vcc = p_branch BB6
BB5
/* logical preds: / linear preds: BB3, / kind: uniform, */
         s2: %25:vcc = p_branch BB6
BB6
/* logical preds: BB1, BB4, / linear preds: BB4, BB5, / kind: uniform, top-level, merge, export_end, */
         v1: %8:v[0] = p_phi 0x3f666666, 0x3dcccccd
         s2: %36:exec = p_parallelcopy %30:s[0-1]
        p_logical_start
         v1: %26:v[0] = v_cvt_pkrtz_f16_f32 %8:v[0], %8:v[0]
        exp %26:v[0], %26:v[0],  v1: undef,  v1: undef en:rg** mrt0
        p_logical_end
        s_endpgm
`

export const templates: { name: string, code: string }[] = [
  { name: "Simple branch", code: simpleBranch },
  { name: "Loop", code: loop },
]
