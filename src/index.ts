import { renderCFG } from './render'
import { templates } from './shader_templates'
import { parseACOIR, parseDisassembly } from './radv_parser'
import { compressToEncodedURIComponent, decompressFromEncodedURIComponent } from 'lz-string'

document.body.style["margin"] = "0"

const root = document.getElementById("root")!
root.style["height"] = "100%"

const outerFlexbox = document.createElement("div")
outerFlexbox.style["display"] = "flex"
outerFlexbox.style["flexDirection"] = "column"
outerFlexbox.style["height"] = "100%"

const fossilUploader = document.createElement("input")
fossilUploader.id = "fossil-file"
fossilUploader.type = "file"
fossilUploader.accept = ".foz"
fossilUploader.hidden = true

const fossilUploaderButton = document.createElement("button")
fossilUploaderButton.appendChild(document.createTextNode("Load Pipelines..."))
fossilUploaderButton.addEventListener("click", _ => {
  fossilUploader.click()
})
// Also allow loading fossils by drag'n drop onto the site body
document.body.ondragover = (ev: any) => {
  ev.preventDefault();
}
document.body.ondrop = (ev: DragEvent) => {
  // Ignore drops of non-fossil files
  if (ev.dataTransfer?.files.length != 1) {
    return
  }

  // Also ignore if a fossil is already loaded
  if (fossilUploaderButton.hidden) {
    return
  }

  onFossilLoaded(ev.dataTransfer!.files[0])
  ev.preventDefault();
}

const shaderTemplateSelector = document.createElement("select")
shaderTemplateSelector.name = "shaderTemplate"
templates.forEach(template => {
  const option = document.createElement("option")
  option.appendChild(document.createTextNode(template.name))
  shaderTemplateSelector.options.add(option)
})

const pipelineSelector = document.createElement("select")
pipelineSelector.name = "shaderTemplate"
{
  const option = document.createElement("option")
  option.appendChild(document.createTextNode("(Pipelines)"))
  pipelineSelector.options.add(option)
}
pipelineSelector.disabled = true
pipelineSelector.hidden = true

const shaderStageSelector = document.createElement("select")
{
  const option = document.createElement("option")
  option.appendChild(document.createTextNode("(Shader Stage)"))
  shaderStageSelector.options.add(option)
}
shaderStageSelector.disabled = true
shaderStageSelector.hidden = true

const fitButton = document.createElement("button")
fitButton.innerHTML = "Fit"
const zoomOutButton = document.createElement("button")
zoomOutButton.innerHTML = "-"
const zoomInButton = document.createElement("button")
zoomInButton.innerHTML = "+"

const cfgLogical = document.createElement("input")
cfgLogical.type = "radio"
cfgLogical.name = "cfgType"
cfgLogical.value = "logical"
const cfgLinear = document.createElement("input")
cfgLinear.type = "radio"
cfgLinear.name = "cfgType"
cfgLinear.value = "linear"
const cfgBoth = document.createElement("input")
cfgBoth.type = "radio"
cfgBoth.name = "cfgType"
cfgBoth.value = "both"
cfgBoth.checked = true
const cfgLogicalLabel = document.createElement("label")
const cfgLinearLabel = document.createElement("label")
const cfgBothLabel = document.createElement("label")
cfgLogicalLabel.appendChild(cfgLogical)
cfgLinearLabel.appendChild(cfgLinear)
cfgBothLabel.appendChild(cfgBoth)
cfgLogicalLabel.appendChild(document.createTextNode("Logical"))
cfgLogicalLabel.style["color"] = "#1874cd"
cfgLinearLabel.appendChild(document.createTextNode("Linear"))
cfgLinearLabel.style["color"] = "#228b22"
cfgBothLabel.appendChild(document.createTextNode("Both"))

const cfgLangSelector = document.createElement("select")
cfgLangSelector.name = "cfgLang"
for (const lang of ["ACO IR", "Disassembly"]) {
  const option = document.createElement("option")
  option.value = lang
  option.appendChild(document.createTextNode(lang))
  cfgLangSelector.options.add(option)
}
{
  const option = document.createElement("option")
  option.value = "unknown"
  option.appendChild(document.createTextNode("(unknown)"))
  option.style["display"] = "none"
  cfgLangSelector.options.add(option)
}
cfgLangSelector.style["flexBasis"] = "16em"
shaderTemplateSelector.style["flexBasis"] = "12em"
pipelineSelector.style["flexBasis"] = "12em"
shaderStageSelector.style["flexBasis"] = "12em"

const saveButtonLink = document.createElement("a")
const saveButton = document.createElement("button")
saveButtonLink.download = "cfg.svg"
saveButtonLink.type = "application/svg+xml"
saveButton.innerHTML = "💾"
saveButtonLink.appendChild(saveButton)

const shareButton = document.createElement("button")
shareButton.innerHTML = "Share"

const toolbarOuter = document.createElement("div")
toolbarOuter.style["display"] = "flex"
toolbarOuter.style["alignItems"] = "center"
toolbarOuter.style["justifyContent"] = "space-between"
toolbarOuter.style["padding"] = "0 0.2em"
toolbarOuter.style["borderBottom"] = "1px solid darkgray"
const toolbar = document.createElement("div")
toolbar.style["display"] = "flex"
toolbar.style["alignItems"] = "center"
toolbar.style["justifyContent"] = "center"
toolbar.style["marginTop"] = "0.2em"
toolbar.style["marginBottom"] = "0.2em"

const inputShaderDiv = document.createElement("div")
inputShaderDiv.appendChild(shaderTemplateSelector)
inputShaderDiv.appendChild(pipelineSelector)
inputShaderDiv.appendChild(shaderStageSelector)
inputShaderDiv.appendChild(cfgLangSelector)
fossilUploaderButton.style["marginLeft"] = "1em"
inputShaderDiv.appendChild(fossilUploaderButton)
inputShaderDiv.appendChild(fossilUploader)
inputShaderDiv.style["display"] = "flex"
inputShaderDiv.style["gap"] = "0.2em"
inputShaderDiv.style["flexGrow"] = "1"
toolbarOuter.appendChild(inputShaderDiv)
toolbar.appendChild(zoomOutButton)
toolbar.appendChild(fitButton)
toolbar.appendChild(zoomInButton)
toolbar.appendChild(cfgLogicalLabel)
toolbar.appendChild(cfgLinearLabel)
toolbar.appendChild(cfgBothLabel)
toolbar.appendChild(saveButtonLink)
toolbar.appendChild(shareButton)
saveButton.style["marginLeft"] = "1em"
shareButton.style["marginLeft"] = "0.2em"
fitButton.style["margin"] = "0em 0.2em"
fitButton.style["width"] = "2.5em"
zoomInButton.style["marginRight"] = "1em"
zoomInButton.style["width"] = "1.5em"
zoomOutButton.style["width"] = "1.5em"
toolbarOuter.appendChild(toolbar)
outerFlexbox.appendChild(toolbarOuter)

const flexbox = document.createElement("div")
flexbox.style["display"] = "flex"
flexbox.style["flex"] = "1"
flexbox.style["overflow"] = "auto"

const textArea = document.createElement("textarea")
textArea.style["width"] = "25%"
textArea.style["resize"] = "horizontal"
textArea.style["margin"] = "0px"
textArea.spellcheck = false

const cfg = document.createElement("div")
cfg.style["flex"] = "1";
cfg.style["overflow"] = "auto";
cfg.style["alignSelf"] = "top"
cfg.style["backgroundColor"] = "#fcffff"

flexbox.appendChild(textArea)
flexbox.appendChild(cfg)
outerFlexbox.appendChild(flexbox)
root.appendChild(outerFlexbox)

function registerCFGMouseOverHandlers(cfg: Element) {
  // On mouse-hover, highlight all occurrences of the hovered register/temporary
  for (const node of cfg.querySelectorAll(`[id^="node"]`)) {
    for (const child of node.querySelectorAll(`text[text-anchor*="start"]`)) {
      let register_regexps = []
      register_regexps.push("[sv][0-9]+(?!\\:)")      // SGPRs/VGPRs in disasm output (with register class indicators like "v2:" excluded)
      register_regexps.push("[sv]\\[\\d+(-\\d+)?\\]") // SGPR/VGPR ranges in disasm output
      register_regexps.push("[sv]\\[[0-9]+:\\d+\\]")  // SGPR/VGPR allocations in ACO IR
      register_regexps.push("vcc|scc|exec|m0")        // Miscellaneous registers
      register_regexps.push("attr\\d+\\.[w-z]")       // v_interp_ argument
      register_regexps.push("%\\d+")                  // ACO IR temporaries
      const regexp = RegExp(`(${register_regexps.join('|')})(?=[,\\s:])`, "g")
      let match
      let matches = []
      while ((match = regexp.exec(child.innerHTML))) {
        matches.push(match)
      }

      if (matches.length) {
        let new_html = child.innerHTML

        // Iterate in reverse order to make sure we don't invalidate the match indexes
        matches.reverse()
        for (const match of matches) {
          const range = match[0].match(RegExp(/\[(\d+)([:-](\d+))?\]/))

          let regs = []
          if (range) {
            const lo = parseInt(range[1])
            const hi = range[3] ? parseInt(range[3]) : lo
            for (let idx = lo; idx <= hi; idx++) {
              // Replace range with current index,
              // mapping e.g. "s[1:3]" to "s1", "s2", and "s3"
              regs.push(match[0].slice(0, range.index!) + idx.toString() + match[0].slice(range.index! + range[0].length))
            }
          } else {
            regs.push(match[0])
          }

          const to_class_name = (reg:string) => {
            console.assert(!reg.includes("[") && !reg.includes("]") && !reg.includes(":"), "Invalid register name: " + reg)
            return "register-" + reg.replace("%", "temp")
          }
          const class_names = regs.map(to_class_name).join(" ")
          new_html = new_html.slice(0, match.index) + `<tspan class="register ${class_names}">${match[0]}</tspan>` + new_html.slice(match.index + match[0].length)
        }
        child.innerHTML = new_html
      }
    }
  }

  const onhover = (ev: Event) => {
    for (const class_name of (ev.target as Element).classList) {
      if (class_name.startsWith("register-")) {
        for (const reg of cfg.getElementsByClassName(class_name)) {
          (reg as HTMLElement).style["fontWeight"] = "bold"
        }
      }
    }
  }
  const onunhover = (ev: Event) => {
    for (const class_name of (ev.target as Element).classList) {
      if (class_name.startsWith("register-")) {
        for (const reg of cfg.getElementsByClassName(class_name)) {
          (reg as HTMLElement).style["fontWeight"] = ""
        }
      }
    }
  }

  for (const child of cfg.getElementsByClassName(`register`)) {
    (child as HTMLElement).style["cursor"] = "pointer"
    child.addEventListener("mouseover", onhover)
    child.addEventListener("mouseout", onunhover)
  }
}

function updateCFG(shader: string): Promise<void> {
  const sections = shader.split("Representation: ").filter(section => section.length > 0)

  // Section headers as written by vkGetShaderInfoAMD
  const aco_marker = "ACO IR (The ACO IR after some optimizations)\n"
  const disasm_marker = "Assembly (Final Assembly)\n"

  // Parse shader code starting from section markers; if none are found, attempt to parse the entire input instead
  const strip_aco_header = (shader: string) => {
    const match = shader.match("\nACO shader stage: .*\n")
    if (!match) {
      return ""
    }
    return shader.slice(match[0].length)
  }
  let aco_code = sections.find(section => section.startsWith(aco_marker))?.slice(aco_marker.length)
  aco_code = aco_code ? strip_aco_header(aco_code) : shader
  const disasm_code = sections.find(section => section.startsWith(disasm_marker)) ?? shader

  const aco_blocks = parseACOIR(aco_code)
  const disasm_blocks = parseDisassembly("Representation: " + disasm_code, aco_blocks)

  let has_data = false
  for (const [name, available] of [
        ["ACO IR", aco_blocks.length > 0],
        ["Disassembly", aco_blocks.length > 0 || disasm_blocks.length > 0]] as const)
  {
    const option = Array.from(cfgLangSelector.options).find(option => option.value === name)!
    if (available) {
      option.style["display"] = ""
    }
    if (!available) {
      option.style["display"] = "none"
    }

    if (name === cfgLangSelector.value) {
      has_data = available
    }
  }

  if (!has_data) {
    // Select the first input format that was successfully parsed (if any)
    const option = Array.from(cfgLangSelector.options).find(option => option.style["display"] !== "none")
    if (option) {
      cfgLangSelector.value = option ? option.value : "unknown"
      cfgLangSelector.disabled = false
    } else {
      cfgLangSelector.options[cfgLangSelector.length - 1].style["display"] = "none"
      cfgLangSelector.value = "unknown"
      cfgLangSelector.disabled = true

      cfg.innerHTML = `<div style="display: flex; justify-content: center; align-items: center; height: 100%; filter: grayscale(30%)"><h1>😩</h1></div>`
      return Promise.resolve(undefined)
    }
  }

  // TODO: If neither ACO IR nor GCN/RDNA assembly was detected, fill CFG area with a text explaining what format is expected

  const blocks = [aco_blocks, disasm_blocks]
  return renderCFG( blocks[cfgLangSelector.selectedIndex],
                    cfgLogical.checked || cfgBoth.checked,
                    cfgLinear.checked || cfgBoth.checked).then(result => {
    if (result == undefined) {
      return Promise.resolve()
    }

    cfg.innerHTML = result

    registerCFGMouseOverHandlers(cfg)

    // Center contained SVG element
    const svg = cfg.firstElementChild as HTMLElement
    svg.style["display"] = "block";
    svg.style["margin"] = "auto"

    // Patch up block header positions, which can't be configured in graphviz
    // and which are placed at awkward default positions
    for (const child of cfg.querySelectorAll(`text[text-anchor*="middle"]`)) {
      // Get the leftmost x coordinate of the rect drawn around block instructions
      const containerBounds = child.parentElement?.getElementsByTagName("polygon")[0].getAttribute("points")?.split(' ').map(coords => coords.split(',')[0]).map(parseFloat)
      if (containerBounds && containerBounds?.length > 0) {
        const leftBound = Math.min.apply(Math, containerBounds)
        child.setAttribute("text-anchor", "start")
        child.setAttribute("x", leftBound.toString())
      }
    }

    // The renderer needs to append dummy spaces on each line to workaround
    // limitations of Graphviz's SVG driver. Let's clean that up here.
    for (const child of cfg.querySelectorAll(`text`)) {
      child.lastChild?.replaceWith((child.lastChild as Text).wholeText.trimEnd())
    }
  }).then(_ => {});
}

function scrollToEntryBlock() {
  const node_element = cfg.querySelector("g #node1")
  const border_element = node_element?.getElementsByTagName("polygon")[0]
  if (node_element && border_element) {
    // Bring the center of the block to the horizontal center of the scroll area
    let bbox = border_element.getBoundingClientRect()
    cfg.scrollLeft = cfg.scrollLeft + bbox.left + bbox.width / 2 - cfg.getBoundingClientRect().x - cfg.clientWidth / 2

    // Bring the top of the block to the upper sixth of the scroll area
    let top = node_element!.getBoundingClientRect().y
    cfg.scrollTop = cfg.scrollTop + top - cfg.getBoundingClientRect().y - cfg.clientHeight / 6
  }
}

const worker = new Worker(new URL("./wasm_worker.ts", import.meta.url))
worker.onmessage = (e: { data: { type: string } }) => {
  console.log(`Received response of type ${e.data.type} from web worker`)
  if (e.data.type == "num_pipelines") {
    const data = e.data as unknown as {
      num_graphics_pipelines: number,
      num_compute_pipelines: number
      }
    pipelineSelector.options.length = 0
    for (const type of ["Graphics", "Compute"]) {
      for (let i = 0; i < data.num_graphics_pipelines; i++) {
        const option = document.createElement("option")
        option.appendChild(document.createTextNode(`${type} Pipeline ${i}`))
        pipelineSelector.options.add(option)
      }
    }
    pipelineSelector.disabled = false

    onPipelineSelected()
  } else if (e.data.type == "stages") {
    const data = e.data as unknown as { stages: string[] }
    const previous_selection = shaderStageSelector.value
    shaderStageSelector.options.length = 0
    for (const stage of data.stages) {
      const option = document.createElement("option")
      option.appendChild(document.createTextNode(`${stage} Shader`))
      option.value = stage
      shaderStageSelector.options.add(option)
    }

    // Reselect previous shader stage if it's still present,
    // otherwise fall back to the first one available
    let new_selection = shaderStageSelector.options[0].value
    for (const item of shaderStageSelector.options) {
      if (item.value == previous_selection) {
        new_selection = item.value
      }
    }
    shaderStageSelector.value = new_selection

    onShaderStageSelected()
    shaderStageSelector.disabled = false
  } else if (e.data.type == "disasm") {
    const data = e.data as unknown as { disasm: string }
    textArea.value = data.disasm
    textArea.disabled = false
    updateCFG(textArea.value).then(scrollToEntryBlock)
  } else if (e.data.type == "disasm_from_ir") {
    const data = e.data as unknown as { disasm?: string, error?: string }
    if (data.disasm) {
      updateCFG(textArea.value + "\ndisasm:\n" + data.disasm)
    } else {
      cfg.innerHTML = `<div style="display: flex; justify-content: center; align-items: center; height: 100%; filter: grayscale(30%); flex-direction: column"><h1>😩</h1><p>${data.error!}</p></div>`
    }
  } else {
    console.assert(false, "Unknown worker message type")
  }
}

function onPipelineSelected() {
  shaderStageSelector.disabled = true
  cfgLangSelector.disabled = true
  const data = { type: "select_pipeline", index: pipelineSelector.selectedIndex }
  worker.postMessage(data)
}

function triggerCompile() {
  textArea.disabled = true
  textArea.value = "Compiling shader..."

  const data = { type: "compile", index: pipelineSelector.selectedIndex, stage: shaderStageSelector.value }
  worker.postMessage(data)
}

function onShaderStageSelected() {
  cfgLangSelector.disabled = false
  triggerCompile()
}

function onTemplateSelected() {
  setDummyTemplateOptionVisible(false)

  textArea.value = templates.find(({name: name }) => name == shaderTemplateSelector.value)!.code

  onShaderCodeChanged(true)
}

const load_acoir_marker = "#load-acoir="
const load_acoir_from_url_marker = "#load-acoir-from="
const load_disasm_marker = "#load-disasm="
const load_disasm_from_url_marker = "#load-disasm-from="

function updateClientRoute() {
  const from_fossilize = (window.location.hash == "#from-fossilize")

  shaderTemplateSelector.hidden = from_fossilize
  textArea.readOnly = from_fossilize

  if (from_fossilize) {
    return
  }

  const load_acoir = window.location.hash.startsWith(load_acoir_marker)
  const load_acoir_from_url = window.location.hash.startsWith(load_acoir_from_url_marker)
  const load_disasm = window.location.hash.startsWith(load_disasm_marker)
  const load_disasm_from_url = window.location.hash.startsWith(load_disasm_from_url_marker)
  if (load_acoir || load_disasm) {
    const encodedShader = window.location.hash.slice(load_disasm ? load_disasm_marker.length : load_acoir_marker.length)
    textArea.value = decompressFromEncodedURIComponent(encodedShader) ?? ""
    cfgLangSelector.value = load_disasm ? "Disassembly" : "ACO IR"
    onShaderCodeChanged(true)
    setDummyTemplateOptionVisible(true)
  } else if (load_acoir_from_url || load_disasm_from_url) {
    var req = new XMLHttpRequest
    req.responseType = 'blob'
    req.open("GET", window.location.hash.slice(load_disasm_from_url ? load_disasm_from_url_marker.length : load_acoir_from_url_marker.length))
    req.onload = () => {
      req.response.text().then((contents: string) => {
        textArea.value = contents
        cfgLangSelector.value = load_disasm_from_url ? "Disassembly" : "ACO IR"
        onShaderCodeChanged(true)
        setDummyTemplateOptionVisible(true)
        })
      }
    req.send()
  } else {
    onTemplateSelected()
  }
}

if (window.location.hash == "#from-fossilize") {
  window.location.hash = ""
}

updateClientRoute()

window.addEventListener("hashchange", _ => updateClientRoute())

shaderTemplateSelector.addEventListener("change", _ => onTemplateSelected())

pipelineSelector.addEventListener("change", _ => onPipelineSelected())

shaderStageSelector.addEventListener("change", _ => onShaderStageSelected())

function setDummyTemplateOptionVisible(visible: Boolean) {
  // Display a dummy option when the shader code has been modified. This option
  // is added at the top and is the only disabled option.

  if (visible && !shaderTemplateSelector.options[0].disabled) {
    const option = document.createElement("option")
    option.disabled = true
    option.selected = true
    option.value = ""
    option.appendChild(document.createTextNode("Load Template..."))
    shaderTemplateSelector.options.add(option, 0)
    shaderTemplateSelector.selectedIndex = 0
  }

  if (!visible && shaderTemplateSelector.options[0].disabled) {
    shaderTemplateSelector.options.remove(0)
  }
}

textArea.addEventListener("input", function (this: HTMLTextAreaElement, e: Event) {
  setDummyTemplateOptionVisible(true)

  onShaderCodeChanged(false)
})

const onCfgChanged = (_: Event) => updateCFG(textArea.value)
cfgBoth.addEventListener("change", onCfgChanged)
cfgLinear.addEventListener("change", onCfgChanged)
cfgLogical.addEventListener("change", onCfgChanged)

function onShaderCodeChanged(scroll_to_entry: Boolean) {
  const promise = updateCFG(textArea.value)

  if (scroll_to_entry) {
    promise.then(scrollToEntryBlock)
  }
  if (cfgLangSelector.value == "Disassembly" && (textArea.value.search("disasm:") == -1 && textArea.value.search("Representation: Assembly") == -1)) {
    const data = { type: "compile_ir", aco_ir: textArea.value }
    worker.postMessage(data)
  }
}

cfgLangSelector.addEventListener("change", (e: Event) => {
  onShaderCodeChanged(true)
})

fossilUploader.addEventListener("change", (e: Event) => {
  onFossilLoaded(fossilUploader.files![0])
})

function onFossilLoaded(fossil: File) {
  window.location.hash = "#from-fossilize"

  fossilUploaderButton.hidden = true
  shaderTemplateSelector.hidden = true
  pipelineSelector.hidden = false
  shaderStageSelector.hidden = false
  cfgLangSelector.disabled = true
  cfgLangSelector.hidden = false
  cfgLangSelector.value = "ACO IR"

  textArea.disabled = true
  textArea.value = "Processing database..."

  var data = { type: "opendb", file: fossil }
  worker.postMessage(data)
}

function onRequestResize(factor: number) {
  const cfgArea = cfg.firstElementChild
  if (!cfgArea) {
    return
  }
  const scale = (val: string) => {
    // Read an attribute like "500pt" and scale it without losing the unit suffix
    const match = RegExp(/([\d.]+)(.*)/).exec(val)!
    return (parseFloat(match[1]!) * factor).toString() + match[2]
  }

  const prevScrollLeft = cfg.scrollLeft
  const prevScrollTop = cfg.scrollTop

  // TODO: Can we use CSS transforms for this instead?
  cfgArea.setAttribute("width", scale(cfgArea.getAttribute("width")!))
  cfgArea.setAttribute("height", scale(cfgArea.getAttribute("height")!))

  // Adjust scroll offset to preserve the focus of the viewport center
  cfg.scrollLeft = prevScrollLeft * factor + ((factor - 1.0) * cfg.clientWidth / 2.0)
  cfg.scrollTop = prevScrollTop * factor + ((factor - 1.0) * cfg.clientHeight / 2.0)
}

fitButton.addEventListener("click", (_: Event) => {
  const cfgArea = cfg.firstElementChild as SVGSVGElement
  const viewBox = cfgArea.viewBox.baseVal
  const aspectRatio = viewBox.width / viewBox.height

  cfgArea.setAttribute("width", cfg.clientWidth.toString())
  cfgArea.setAttribute("height", (cfgArea.width.baseVal.value / aspectRatio).toString())
})
zoomOutButton.addEventListener("click", (_: Event) => {
  onRequestResize(0.9)
})
zoomInButton.addEventListener("click", (_: Event) => {
  onRequestResize(1.0/0.9)
})

window.addEventListener('keydown', (event: KeyboardEvent) => {
  if (event.key == '+') {
    onRequestResize(1.0/0.9)
    event.preventDefault()
  } else if (event.key == '-') {
    onRequestResize(0.9)
    event.preventDefault()
  }
})

shareButton.addEventListener("click", (e: Event) => {
  window.location.hash = ((cfgLangSelector.value == "Disassembly") ? load_disasm_marker : load_acoir_marker) + compressToEncodedURIComponent(textArea.value)
})

saveButton.addEventListener("click", _ => {
  const data = new Blob([cfg.innerHTML.replace(/&nbsp;/g, " ")])
  saveButtonLink.href = URL.createObjectURL(data)
})
