const radv = require('./radv_ems')()

let ctx: any

// Mask values correspond to VkShaderStageFlagBits
const shader_stages = [
  { mask: 0x001, name: "Vertex" },
  { mask: 0x002, name: "Tessellation Control" },
  { mask: 0x004, name: "Tessellation Evaluation" },
  { mask: 0x008, name: "Geometry" },
  { mask: 0x010, name: "Fragment" },
  { mask: 0x020, name: "Compute" },
]

self.onmessage = function(e: { data: { type: string } }) {
  if (e.data.type == "opendb") {
    const data = e.data as unknown as { file: File }
    radv.then((instance: any) => {
      instance.FS.mkdir("/workerfs")
      instance.FS.mount(instance.WORKERFS, {
        blobs: [],
        files: [data.file],
      }, '/workerfs');

      ctx = instance.ccall("fos_create_context", 'void*', ['string'], ["/workerfs/" + data.file.name]);
      const message_num_pipelines = {
        type: "num_pipelines",
        num_graphics_pipelines: instance.ccall("count_graphics_pipelines", 'int', ['void*'], [ctx]),
        num_compute_pipelines: instance.ccall("count_compute_pipelines", 'int', ['void*'], [ctx])
      }
      // @ts-ignore
      postMessage(message_num_pipelines)

      let err = instance.ccall("prepare_fos_db", 'string', ['void*'], [ctx])
      // TODO: Handle error
    })
  } else if (e.data.type == "select_pipeline") {
    const data = e.data as unknown as { index: number }
    radv.then((instance: any) => {
      const stage_mask = instance.ccall("fos_get_active_shader_stages", 'number', ['void*', 'number'], [ctx, data.index])

      const active_stages = shader_stages.filter(stage => (stage_mask & stage.mask) != 0).map(stage => stage.name)

      const message = {
        type: "stages",
        stages: active_stages
      }
      // @ts-ignore
      postMessage(message)
    })
  } else if (e.data.type == "compile") {
    const data = e.data as unknown as { index: number, stage: string }

    const stage_mask = shader_stages.find(stage => stage.name == data.stage)!.mask

    radv.then((instance: any) => {
      const disasm = instance.ccall("fosmain", 'string', ['void*', 'number', 'number'], [ctx, data.index, stage_mask])

      const ret = {
        type: "disasm",
        disasm: disasm
      }
      // @ts-ignore
      postMessage(ret)
    })
  } else if (e.data.type == "compile_ir") {
    const data = e.data as unknown as { aco_ir: string }
    radv.then((instance: any) => {
      try {
        const result = compile_aco_ir(instance, data.aco_ir)
        const ret = {
          type: "disasm_from_ir",
          disasm: result
        }
        // @ts-ignore
        postMessage(ret)
      } catch (e) {
        const ret = {
          type: "disasm_from_ir",
          error: e.message
        }
        // @ts-ignore
        postMessage(ret)
      }
    })
  } else {
    console.assert(false, "Unknown message type for web worker")
  }
};

function compile_aco_ir(instance: any, shader: string) {
  let failed_instr: string | undefined

  try {
    var ctx = instance.ccall("aco_ems_begin", 'void*', [], []);

    const regex = `^(?=BB\\d+\n/\\* logical preds: (?:BB\\d+, )*/ linear preds: (?:BB\\d+, )*/ kind: .*\\*/\n)`
    const capturing_regex = `(BB\\d+)\n/\\* logical preds: ((?:BB\\d+, )*)/ linear preds: ((?:BB\\d+, )*)/ kind: ((.*, )*)\\*/\n`
    const block_strings = shader.split(RegExp(regex, 'm'))
    for (const block_string of block_strings) {
      const matches = block_string.match(capturing_regex)!
      const get_preds = ((list:string) => { const split = list.split(", "); split.pop(); return split })
      const logical_preds = get_preds(matches[2])
      const linear_preds = get_preds(matches[3])
      const kinds = matches[4].split(", ").slice(0, -1)

      const result = instance.ccall("aco_ems_begin_block", 'bool', ['void*', 'string', 'string', 'string', 'string'],
                [ctx, "TODO", kinds.join(','), logical_preds.join(','), linear_preds.join(',')])
      if (result == false) {
        throw new Error("aco_ems_begin_block returned false")
      }

      const instrs = block_string.split('\n').slice(2)
      for (const instr of instrs) {
        if (instr.length == 0) {
          continue
        }

        const split_instr = instr.split('=')
        const [lhs, rhs] = split_instr.length == 1 ? ["", split_instr[0]] : split_instr
        try {
          const result = instance.ccall("aco_ems_add_instr", 'bool', ['void*', 'string', 'string'], [ctx, lhs, rhs]);
          if (result == false) {
            const error = instance.ccall("aco_ems_get_last_error", 'string', ['void*'], [ctx])
            throw new Error(error)
          }
        } catch (e) {
          failed_instr = (lhs.length ? (lhs.trim() + " = ") : "") + rhs.trim()
          throw e
        }
      }
    }

    return instance.ccall("aco_ems_end", 'string', ['void*'], [ctx]);
  } catch (e) {
    throw new Error(`Shader compilation failed${failed_instr ? ` at instruction "${failed_instr}"` : ""}:<br>${e.message}`)
  }
}
