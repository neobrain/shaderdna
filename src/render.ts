import { Block } from './radv_parser'

// Defined in containing html file
declare const hpccWasm: any;

export function renderCFG(blocks: Block[], logicalEdges: boolean, linearEdges: boolean): Promise<string> {
  let dot = "digraph {\n"
  dot += `graph [bgcolor="#fcffff"]\n`
  dot += `node [fillcolor="#fefefa" style=filled height=0.02 fontname="monospace" fontsize=8]\n`

  // Not all block names make valid graphviz node names, hence replace invalid characters
  const sanitized_block_name = ((name: string) => name.replace(".", "_"))

  // Graphviz's SVG driver uses different font metrics than most browsers and
  // hence sizes the block boxes too small. As a workaround, we just add some
  // spaces at the end of each line.
  const formatBlockLine = ((line: string) => line + " ".repeat(line.length * 0.85))
  const formatBlock = (block: Block) => {
    const border_color = `${block.is_linear_block ? "color=forestgreen" : ""}`
    const xlabel = `xlabel="${block.names.join("|") + ((block.kind.length > 0) ? ": " + block.kind.join() : "")}"`
    const instrs = block.contents.map(formatBlockLine).join("\\l")
    const hide = (block.is_linear_block && !linearEdges)
    const style = hide ? "style=invis" : ""
    return sanitized_block_name(block.names[0]) + `[shape=box ${border_color} ${style} ${xlabel} label="${instrs}\\l"]\n`
  }
  dot += blocks.map(formatBlock).join("")
  dot += "\n"

  dot += "\n// Logical+linear edges\n"
  dot += blocks.map(block => {
    const commonPreds = block.linear_preds.filter(pred => block.logical_preds.includes(pred))
    return commonPreds.map(pred => `${sanitized_block_name(pred)} -> ${sanitized_block_name(block.names[0])}`).join("\n")
  }).join("\n");

  // Render linear/logical edges, if enabled.
  // If they are disabled, render them invisibly to ensure consistent placing of graph element
  dot += "\n// Linear edges\n"
  dot += blocks.map(block => {
    const onlyLinearPreds = block.linear_preds.filter(pred => !block.logical_preds.includes(pred))
    const attribs = `[color=forestgreen fontcolor=forestgreen ${!linearEdges ? "style=invis" : ""}]`
    return onlyLinearPreds.map(pred => `${sanitized_block_name(pred)} -> ${sanitized_block_name(block.names[0])} ${attribs}`).join("\n")
  }).join("\n");

  dot += "\n// Logical edges\n"
  dot += blocks.map(block => {
    const onlyLogicalPreds = block.logical_preds.filter(pred => !block.linear_preds.includes(pred))
    const attribs = `[color=dodgerblue3 fontcolor=dodgerblue3 ${!logicalEdges ? "style=invis" : ""}]`
    return onlyLogicalPreds.map(pred => `${sanitized_block_name(pred)} -> ${sanitized_block_name(block.names[0])} ${attribs}`).join("\n")
  }).join("\n");

  // Rank immediate successors in the logical CFG the same
  const logical_succs = new Map(blocks.map(block => [block.names[0], [] as string[]]))
  blocks.forEach(block => block.logical_preds.forEach(pred => logical_succs.get(pred)!.push(block.names[0])))
  dot += "\n" + blocks.map(block => "{ rank = same;" + logical_succs.get(block.names[0])!.map(pred => sanitized_block_name(pred) + ';').join(' ') + " }").join('\n')

  blocks.filter(block => block.linear_preds.length == 0).forEach(block => dot += `{ rank = source; ${sanitized_block_name(block.names[0])}; }`)
  blocks.filter(block => block.is_end_block).forEach(block => dot += `{ rank = sink; ${sanitized_block_name(block.names[0])}; }`)
  dot += "\n}"

  return hpccWasm.graphviz.layout(dot, "svg", "dot")
}
