import { format_instruction } from './instruction_formatter'

export type Block = {
  names: string[]
  kind: string[]
  contents: string[] // disassembly or ACO IR
  logical_preds: string[]
  linear_preds: string[]
  is_linear_block: boolean // True if this block is only in the linear CFG
  is_end_block: boolean // True if this block ends with s_endpgm
}

function blockNameComparer(name: string) {
  return ((block: Block) => block.names.includes(name))
}

// Find all paths to nonempty blocks in the disassembly CFG.
// This concatenates edges of the original CFG since many blocks will be empty post optimization
function findPredsInDisasm(disasm_blocks: Block[], aco_blocks: Block[], aco_block: Block, kind: string[] = aco_block.kind, set: Map<string, { has_linear_edge: boolean, has_logical_edge: boolean }> = new Map()) {
  const traversePreds = (preds: string[], linear: boolean) => {
    const get_or_create = (name: string) => {
      name = disasm_blocks.find(blockNameComparer(name))!.names[0]
      if (!set.has(name)) {
        set.set(name, { has_linear_edge: false, has_logical_edge: false })
      }
      return set.get(name)
    }
    const mark_logical = (name: string) => { get_or_create(name)!.has_logical_edge = true }
    const mark_linear = (name: string) => { get_or_create(name)!.has_linear_edge = true }

    for (const pred_name of preds) {
      const pred = disasm_blocks.find(blockNameComparer(pred_name))
      if (pred) {
        if (linear) mark_linear(pred_name)
        else mark_logical(pred_name)
      } else {
        const pred = aco_blocks.find(blockNameComparer(pred_name))!
        kind.concat(pred.kind)
        findPredsInDisasm(disasm_blocks, aco_blocks, pred, kind, set)
      }
    }
  }

  traversePreds(aco_block.logical_preds, false)
  traversePreds(aco_block.linear_preds, true)

  return [kind, set] as const
}

export function parseACOIR(log: string): Block[] {
  const binary_marker = "disasm:\n"
  let start_of_binary = log.search(binary_marker)
  if (start_of_binary == -1) {
    start_of_binary = log.length
  }

  const regex = `^(?=BB\\d+\n/\\* logical preds: (?:BB\\d+, )*/ linear preds: (?:BB\\d+, )*/ kind: .*\\*/\n)`
  const capturing_regex = `(BB\\d+)\n/\\* logical preds: ((?:BB\\d+, )*)/ linear preds: ((?:BB\\d+, )*)/ kind: ((.*, )*)\\*/\n`
  const block_strings = log.slice(0, start_of_binary).split(RegExp(regex, 'm'))

  let blocks: Block[] = []

  try {
    blocks = block_strings.map(block_string => {
          const matches = block_string.match(capturing_regex)!
          const get_preds = ((list:string) => { const split = list.split(", "); split.pop(); return split })
          let aco_ir = block_string.split('\n').slice(2)
          for (let line_idx = 0; line_idx < aco_ir.length; line_idx++) {
            const trimmed = aco_ir[line_idx].trimLeft()
            if (trimmed == aco_ir[line_idx]) {
              // If we encounter an empty or a non-indented line, assume we reached the block end
              // TODO: Detect ACO IR more accurately
              if (trimmed.length != 0) {
                console.log(`Closing block at non-instruction line "${aco_ir[line_idx]}"`)
              }
              aco_ir = aco_ir.slice(0, line_idx)
              break
            }
            aco_ir[line_idx] = trimmed.trimRight()
          }

          const logical_preds = get_preds(matches[2])
          const linear_preds = get_preds(matches[3])

          return {
            names: [matches[1]],
            kind: matches[4].split(", ").slice(0, -1),
            contents: ([] as string[]).concat.apply([], aco_ir.map(format_instruction)),
            logical_preds: logical_preds,
            linear_preds: linear_preds,
            // Only in linear CFG if there are no logical predecessors and this is not an entrypoint
            is_linear_block: logical_preds.length == 0 && linear_preds.length != 0,
            is_end_block: block_string.trim().endsWith("s_endpgm")
          }
        })
  } catch (e) {
    console.log("Could not parse input as ACO IR, trying as GCN/RDNA assembly...")
  }

  return blocks
}

export function parseDisassembly(log: string, aco_blocks?: Block[]): Block[] {
  if (!aco_blocks) {
    aco_blocks = []
  }

  // ACO IR is optionally followed by the generated GCN/RDNA shader assembly

  let binary_marker = "disasm:\n"
  let start_of_binary = log.indexOf(binary_marker)
  if (start_of_binary == -1) {
    binary_marker = "Representation: Assembly (Final Assembly)\n\n"
    start_of_binary = log.indexOf(binary_marker)
  }
  if (start_of_binary == -1) {
    start_of_binary = log.length
  }

  const strip_empty_tail = (lines: string[]) => {
    // Drop empty lines at the bottom
    while (lines.length > 0 && lines[lines.length - 1] == "") {
      lines.pop()
    }
    return lines
  }

  let blocks: Block[] = []

  if (start_of_binary != log.length) {
    // LLVM disassembly uses the block name format "BB123". CLRX uses "".L52_0".
    // Furthermore, LLVM uses "main" to label the entry point
    const basic_block_regex = `^((\\.?[BL_\\d]+\\d+)|main):`
    let lines = strip_empty_tail(log.slice(start_of_binary + binary_marker.length).split('\n'))

    const stripComment = (line: string) => {
      const semicolon = line.search(';')
      if (semicolon != -1) return line.slice(0, semicolon)
      const slashes = line.search('//')
      if (slashes != -1) return line.slice(0, slashes)
      return line
    }

    // Detect block labels from input

    // is_virtual indicates if the block comes from input (false) or was added by us (true)
    let block_labels = [] as { line_idx: number, name: string, is_virtual: Boolean }[]
    const block_starting_lines: Set<number> = new Set
    let next_unique_block_id = 0
    const append_block_label = (line_idx: number, name?: string, is_virtual?: Boolean) => {
      if (name == undefined) {
        name = `Unlabeled_${next_unique_block_id}`
        next_unique_block_id++
      }

      if (is_virtual == undefined) {
        is_virtual = true
      }

      block_labels.push({ line_idx: line_idx, name: name, is_virtual: is_virtual })

      block_starting_lines.add(line_idx)
    }
    for (let line_idx = 0; line_idx < lines.length; line_idx++) {
      const matching_block = lines[line_idx].match(basic_block_regex)
      if (!matching_block) {
        continue
      }

      const block_name = matching_block[1]
      append_block_label(line_idx, block_name, false)
    }

    // Reconstruct basic blocks if assembly without block labels is given.
    {
      let branch_target_addresses: Set<number> = new Set

      // Detect branches to fixed offsets, and patch them with the target block names
      // (generating new names if none are specified in the input)
      for (const instr_index in lines) {
        const line = lines[instr_index]
        const offset = line.match(RegExp("s_c?branch_?[a-z0-1]*\\s+(\\d+).*// ([0-9A-F]+)"))

        if (offset && offset.length == 3) {
          const instr_offset = parseInt(offset[2], 16)
          // TODO: Actually need to do a sign extension from S16 to S32!
          if (parseInt(offset[1]) > 0x7fff) {
            throw new Error("Sign-extension on branches not supported, yet");
          }
          const target = instr_offset + 4 + 4 * parseInt(offset[1])
          branch_target_addresses.add(target)

          // Replace offset with the block name assigned below
          lines[instr_index] = lines[instr_index].slice(0).replace(
            /(s_c?branch_?[a-z0-1]*\s)(\d+)(\s.*)/,
            `$1block_0x${target.toString(16)} $3`)
        }
      }

      // Map instruction offsets to line numbers.
      // This requires the input to have offset annotations as printed by AMDVLK
      branch_target_addresses.forEach((offset: number) => {
        const first_line = lines.findIndex(line => line.includes("// " + ("0".repeat(12) + offset.toString(16).toUpperCase()).slice(-12)))
        console.assert(first_line != -1, `Could not lookup block start line from instruction offset ${offset.toString(16)}`)
        // TODO: branch instructions are patched up against the name we assign here, but if a label already exists here we should use that instead
        console.assert(!block_starting_lines.has(first_line))
        append_block_label(first_line, `block_0x${offset.toString(16)}`, true)
      })

      // Add block at the top
      if (!block_starting_lines.has(0)) {
        append_block_label(0)
      }

      // Make sure there is a new block after each branch instruction
      for (let line_index = 0; line_index < lines.length; line_index++) {
        const line = lines[line_index]
        if (block_starting_lines.has(line_index + 1)) {
          continue
        }

        const is_branch = RegExp("s_c?branch_?[a-z0-1]* ").test(line)
        if (is_branch && line_index + 1 < lines.length) {
            append_block_label(line_index + 1)
        }
      }
    }

    // Sort blocks and group those that start at the same instruction
    const sorted_labels = Array.from(block_labels).sort((label1, label2) => label1.line_idx - label2.line_idx)

    let block_starts = [] as [number, { name: string, is_virtual: Boolean }[]][]
    for (let block_idx = 0; block_idx < sorted_labels.length;) {
      let end_idx = block_idx
      while ( end_idx + 1 < sorted_labels.length &&
              sorted_labels[end_idx + 1].line_idx - sorted_labels[end_idx].line_idx <= (sorted_labels[end_idx].is_virtual ? 0 : 1)) {
        end_idx++;
      }

      const merged_names = sorted_labels.slice(block_idx, end_idx + 1)
      block_starts.push([ sorted_labels[block_idx].line_idx, merged_names ])

      block_idx = end_idx + 1
    }

    // Collect instructions per block (the block end is determined by the start of each block's successor)
    for (const block_idx of block_starts.keys()) {
      const detected = block_starts[block_idx]
      const end = (block_idx == block_starts.length - 1) ? undefined : block_starts[block_idx + 1][0]

      // Remove comments and filter out assembler directives (starting with ".")
      const num_block_labels = detected[1].filter(({ is_virtual: is_virtual }) => !is_virtual).length
      const instrs = strip_empty_tail(lines.slice(detected[0] + num_block_labels, end).map(instr => stripComment(instr).trim())).filter(line => !line.startsWith("."))

      const block_names = detected[1].map(b => b.name)
      const aco_block = aco_blocks.find(prev_block => block_names.some(block_name => blockNameComparer(block_name)(prev_block)))

      blocks.push({
        names: block_names,
        kind: aco_block?.kind ?? [],
        contents: instrs,
        logical_preds: [], // initialized later
        linear_preds: [],  // initialized later
        is_linear_block: true, // updated later
        is_end_block: instrs.includes("s_endpgm")
      })
    }

    blocks.forEach(block => console.assert(block.contents.length > 0, `Encountered zero-length block ${block.names}`))

    // Infer CFG edges
    for (const block_idx in blocks) {
      const block = blocks[block_idx]

      // Import logical edge information from the ACO IR CFG
      const matching_aco_blocks = aco_blocks.filter(prev_block => block.names.some(block_name => blockNameComparer(block_name)(prev_block)))
      if (matching_aco_blocks.length != 0) {
        let logical_preds: Set<string> = new Set
        let kinds: Set<string> = new Set
        for (const aco_block of matching_aco_blocks) {
          const [new_kind, new_preds] = findPredsInDisasm(blocks, aco_blocks, aco_block)
          for (const [name, edges] of new_preds) {
            if (edges.has_logical_edge) {
              logical_preds.add(name)
            }
          }
          new_kind.forEach(kind => kinds.add(kind))
        }
        block.logical_preds = Array.from(logical_preds)
        block.kind = Array.from(kinds)
        block.is_linear_block = block.logical_preds.length == 0 && block.linear_preds.length != 0
        block.is_end_block = matching_aco_blocks.some(block => block.is_end_block)
      }

      // Infer linear edge information from disassembly
      {
        let has_unconditional_branch = block.contents.some((instr: string) => RegExp("s_branch ").test(instr))
        if (!has_unconditional_branch) {
          // Connect the next block in the disassembly to the current one
          const idx = parseInt(block_idx)
          if (idx < blocks.length - 1 && !blocks[idx].is_end_block) {
            blocks[idx + 1].linear_preds.push(block.names[0])
          }
        }

        // TODO: s_setpc, s_call_b64, s_subvector_loop_begin, s_subvector_loop_end

        for (const instr of block.contents) {
          const branches = instr.match(RegExp("s_c?branch_?[a-z0-1]*\\s+(\\w+)"))
          if (branches && branches.length > 1) {
            blocks.find(blockNameComparer(branches[1]))!.linear_preds.push(block.names[0])
          }
        }
      }
    }
  }

  return blocks
}
