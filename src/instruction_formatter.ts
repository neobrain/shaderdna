export function format_instruction(instr: string): string[] {
  const default_ret = [" ".repeat(19) + instr]

  if (RegExp('^[vs].+: %').test(instr)) {
    let separator_pos = instr.search("=")
    if (separator_pos == -1) {
      return default_ret
    }

    const defs = instr.slice(0, separator_pos).split(", ")
    const rhs = instr.slice(separator_pos).trim()

    const result = defs.map((def, index) => {
      return def.trim().padEnd(16) + ((index < defs.length - 1) ? "⤵" : rhs)
    })

    return result
  }
  return default_ret
}
