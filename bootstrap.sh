#!/bin/bash
set -e

if ! command -v emcc &> /dev/null
then
    echo "Add emcc to your PATH before running this script."
    exit 1
fi

git submodule update

INSTALLPATH=`realpath external/install`

pushd external/fossilize
git submodule update
mkdir -p build && pushd build
cmake -DCMAKE_TOOLCHAIN_FILE=$(dirname `which emcc`)/cmake/Modules/Platform/Emscripten.cmake \
      -DFOSSILIZE_CLI=OFF -DFOSSILIZE_TESTS=OFF -DFOSSILIZE_VULKAN_LAYER=OFF \
      -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX=$INSTALLPATH \
      .. -G"Ninja"
ninja && ninja install
popd
popd

pushd external/clrx
mkdir -p build && pushd build
# Makefile generator required due to build system errors on CLRX's side: static and shared library targets have the same name
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$INSTALLPATH \
    -DCMAKE_TOOLCHAIN_FILE=$(dirname `which emcc`)/cmake/Modules/Platform/Emscripten.cmake -G"Unix Makefiles"
make -j4 && make install
popd
popd

pushd external/mesa
CMAKE_PREFIX_PATH=$INSTALLPATH/lib/cmake/ meson build \
    -Ddri-drivers= -Dgallium-drivers= -Dvulkan-drivers=amd -Dbuildtype=release --cross-file emscripten.txt \
    -Dzstd=disabled -Dllvm=disabled -Ddefault_library=static -Dbuild-aco-tests=false -Dshader-cache=disabled
pushd build
ninja
popd
popd

rm -f src/aco_ems.js
rm -f public/aco_ems.wasm
cp external/mesa/build/src/amd/vulkan/radv_ems.js src/
cp external/mesa/build/src/amd/vulkan/radv_ems.wasm public/
